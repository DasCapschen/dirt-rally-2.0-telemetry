# DiRT Rally 2.0 Telemetry

Little GTK App written in Rust to display DiRT Rally 2.0 Telemetry Data

### Enable Telemetry
Find `My Documents/My Games/DiRT Rally 2.0/hardwaresettings/hardware_settings_config.xml` and open it in your favourite editor.  
Find `<udp enabled="false" extradata="0" ip="127.0.0.1" port="20777" delay="1" />` and change it to `<udp enabled="true" extradata="3" ip="127.0.0.1" port="20777" delay="1" />`  

### Note to Linux (Proton) Users
The file is located in `~/.local/share/Steam/steamapps/compatdata/690790/pfx/drive_c/users/steamuser/` and then the path as above.  
For plain Wine, look in `~/.wine/drive_c/users/<your username>/` followed by the path above.
use std::net::UdpSocket;
use gtk::prelude::*;
use gio::prelude::*;
use gtk::{Application};
use gdk::prelude::*;

use std::sync::Arc;
use std::sync::Mutex;
use std::sync::mpsc::{self, TryRecvError};

mod data;
use crate::data::*;

fn main() {
    //initialize GTK
    let application = Application::new(
        Some("com.github.gtk-rs.examples.basic"),
        Default::default(),
    ).expect("failed to initialize GTK application");

    //create our socket
    let socket = UdpSocket::bind("127.0.0.1:20777")
        .expect("Couldn't bind UDP Socket on 20777");
    socket.set_nonblocking(true).expect("failed to set socket nonblocking");
    let mut buffer = [0_u8;264];

    //prepare data structure
    let data = Arc::new(Mutex::new(Telemetry::default()));
    let cloned_data = data.clone();
    
    //create a channel to kill the thread
    let (tx, rx) = mpsc::channel();

    //start the thread
    let thread = std::thread::spawn(move || {
        'thread: loop {
            //check if thread should be killed
            match rx.try_recv() {
                Ok(_) | Err(TryRecvError::Disconnected) => {
                    println!("Terminating.");
                    break 'thread;
                }
                Err(TryRecvError::Empty) => {}
            }

            //try to receive data from socket
            match socket.recv(&mut buffer[..]) {
                //if yes, set the data
                Ok(_) => *cloned_data.lock().unwrap() = make_telemetry(&buffer),
                //if WouldBlock (waiting for data), wait 0.01667 seconds
                Err(ref e) if e.kind() == std::io::ErrorKind::WouldBlock => std::thread::sleep(std::time::Duration::from_secs_f64(1.0/60.0)),
                //other errors => something bad happened, cancel
                Err(_) => break 'thread,
            }
        }
    });

    //load ui from glade
    let glade_src = include_str!("ui.glade");
    let builder = gtk::Builder::new_from_string(glade_src);

    //get car widgets
    let label_gear: gtk::Label = builder.get_object("label_gear").unwrap();
    let label_rpm: gtk::Label = builder.get_object("label_rpm").unwrap();
    let progress_rpm: gtk::ProgressBar = builder.get_object("progress_rpm").unwrap();
    let label_speed: gtk::Label = builder.get_object("label_speed").unwrap();

    //get track widgets
    let label_time: gtk::Label = builder.get_object("label_time").unwrap();
    let label_progress: gtk::Label = builder.get_object("label_progress").unwrap();

    //get input widgets
    let progress_clutch: gtk::ProgressBar = builder.get_object("progress_clutch").unwrap();
    let progress_brake: gtk::ProgressBar = builder.get_object("progress_brake").unwrap();
    let progress_throttle: gtk::ProgressBar = builder.get_object("progress_throttle").unwrap();
    let scale_steering: gtk::Scale = builder.get_object("scale_steering").unwrap();

    //get window
    let window: gtk::Window = builder.get_object("window").unwrap();

    //run this whenever the GTK main loop is idle
    gtk::idle_add(move || {
        if !window.is_visible() {
            //stop running this when the window disappears
            return Continue(false);
        }

        //get the data
        let telemetry_data = data.lock().unwrap();

        //set gear and speed
        label_gear.set_label( format!("{:1.0}", telemetry_data.gear).as_str() );
        label_speed.set_label( format!("{:3.0} km/h", telemetry_data.speed * 3.6).as_str() ); //to kmh

        //set timestamp
        let ms = (telemetry_data.lap_time * 1000.0) as u32 % 1000;
        let s = telemetry_data.lap_time as u32 % 60;
        let min = (telemetry_data.lap_time / 60.0) as u32;
        label_time.set_label( format!("{:02}:{:02}.{:03}", min, s, ms).as_str() );

        //set track progress
        let progress = telemetry_data.lap_distance / telemetry_data.track_length;
        label_progress.set_label( format!("{:3.0}%", progress.max(0.0).min(1.0) * 100.0).as_str() );

        //set RPM and RPM Progress (TODO: replace with a circular progress bar)
        label_rpm.set_label( format!("{:4.0} RPM", telemetry_data.rpm * 10.0).as_str() ); //to kmh
        let rpm_percentage = telemetry_data.rpm as f64 / telemetry_data.max_rpm as f64;
        progress_rpm.set_fraction( rpm_percentage.max(0.0).min(1.0) );

        //set input widgets
        progress_clutch.set_fraction( telemetry_data.clutch as f64 );
        progress_brake.set_fraction( telemetry_data.brake as f64 );
        progress_throttle.set_fraction( telemetry_data.throttle as f64 );
        scale_steering.set_value( 50.0 * (telemetry_data.steering + 1.0) as f64 );

        //continue running this next time the main loop is idle
        Continue(true)
    });

    let window: gtk::Window = builder.get_object("window").unwrap();
    //when the application is activated, open the window
    application.connect_activate(move |app| {
        app.add_window(&window); //important, else app closes immediately
        window.show_all(); //display it
    });

    //do magic to make css work
    let css_provider = gtk::CssProvider::new();
    let display = gdk::Display::get_default().unwrap();
    let screen = gdk::Display::get_default_screen(&display);
    gtk::StyleContext::add_provider_for_screen(&screen, &css_provider, gtk::STYLE_PROVIDER_PRIORITY_USER);
    css_provider.load_from_path("src/ui.css").expect("couldn't load css file");

    let window: gtk::Window = builder.get_object("window").unwrap();

    //allow the window to be transparent! (as long as we have a compositor)
    window.set_visual(screen.get_rgba_visual().as_ref());

    //move window to lower right corner and make it always be on top
    let w = screen.get_width();
    let h = screen.get_height();
    let (ww,wh) = window.get_size();
    window.move_(w-ww, h-wh);
    window.set_keep_above(true);

    //run the application
    application.run(&[]);

    //application has been stopped, signal thread to die and wait for it to do so
    tx.send(()).expect("failed to send signal to thread");
    thread.join().expect("failed to join thread");
}

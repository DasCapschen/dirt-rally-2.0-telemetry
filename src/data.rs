#[derive(Copy,Clone,Debug,Default)]
pub struct Vec3 {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

#[derive(Copy,Clone,Debug,Default)]
pub struct Wheels {
    //as per dbox sdk, it is front, then back
    //as per https://docs.google.com/spreadsheets/d/1UTgeE7vbnGIzDz-URRk2eBIPc_LR1vWcZklp7xD9N0Y/edit#gid=0, it is back, then front
    //but it is definitely left, then right
    pub fl: f32, //front left
    pub fr: f32, //front right
    pub bl: f32, //back left
    pub br: f32, //back right
}

#[derive(Copy,Clone,Debug,Default)]
pub struct Telemetry {
    pub time: f32,              // 0 since race started???
    pub lap_time: f32,          // 4 for rally -> total time
    pub lap_distance: f32,      // 8 for rally -> total distance in meters
    pub total_distance: f32,    // 12 in meters
    pub world_position: Vec3,   // 16
    pub speed: f32,             // 28 in meters per second
    pub velocity: Vec3,         // 32 
    pub roll: Vec3,             // 44 right vector, or maybe left(why?) vector
    pub pitch: Vec3,            // 56 forward vector
    pub suspension_pos: Wheels, // 68
    pub suspension_vel: Wheels, // 84
    pub wheel_speed: Wheels,    // 100
    pub throttle: f32,          // 116 0 to 1, 1 = full throttle
    pub steering: f32,          // 120 -1 = full left, 1 = full right ; 540°
    pub brake: f32,             // 124 0 to 1, 1 = hard braking
    pub clutch: f32,            // 128 0 to 1, 1 = driver uses clutch
    pub gear: f32,              // 132 -1 is reverse, else 1 to max_gears
    pub gforce_lat: f32,        // 136
    pub gforce_lon: f32,        // 140
    pub current_lap: f32,       // 144 starts at 0
    pub rpm: f32,               // 148 rpm divided by 10
    pub sli_support: f32,       // 152 always one / unused?
    pub race_position: f32,     // 156 starts at 1
    pub kers_level: f32,        // 160 unused
    pub kers_max: f32,          // 164 unused
    pub drs: f32,               // 168 unused
    pub tc: f32,                // 172 unused
    pub abs: f32,               // 176 unused
    pub fuel_in_tank: f32,      // 180 unused
    pub fuel_capacity: f32,     // 184 unused
    pub in_pit: f32,            // 188 unused
    pub sector: f32,            // 192 starts at 0
    pub time_sector_1: f32,     // 196 where are sector 3 and 4???
    pub time_sector_2: f32,     // 200 these do not get overridden!
    pub brake_temp: Wheels,     // 204 starts negative, lerps toward ambient?
    pub /*?*/wheel_pressure: Wheels,// 220 unused, always 0
    pub completed_laps: f32,    // 236 for rally -> 1 if finished
    pub total_laps: f32,        // 240 for rally -> 1
    pub track_length: f32,      // 244 in meters
    pub last_lap_time: f32,     // 248 for rally -> total time in seconds
    pub max_rpm: f32,           // 252 max rpm divided by 10
    pub idle_rpm: f32,          // 256 idle rpm divided by 10
    pub max_gears: f32,         // 260 
} //264 bytes

union TelemetryArray {
    raw: [u8;264],
    tel: Telemetry,
}

pub fn make_telemetry(buffer: &[u8;264]) -> Telemetry {
    unsafe { TelemetryArray { raw: *buffer }.tel }
}